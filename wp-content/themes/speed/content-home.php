<?php get_header('home'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/plugin/lightslider-master/dist/css/lightslider.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/plugin/lightslider-master/dist/css/lightgallery.min.css">
<div id="section1-parent" class="column is-full">  
            <div id="section1" class="container is-fullhd" style="background-image: url('<?php echo get_bloginfo('template_directory'); ?>/images/img-article-body-section-bg.png');">
                <div  id="mask-opacity" class="container is-fullhd">
                    <h1 class="section1-title">INSPIRED BY SPEED, DRIVEN BY PASSION</h1>
                    <p class="text-section-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>    
                </div>
             </div>          
        </div>
            
            <!-- Slider 1 start -->
            <div id="slider-section1" class=" left-control">
                <h1 class="heading-slider">Featured Article</h1>
                <ul class="lightSlider">
                    <?php 

                    $image = [
                        get_bloginfo('template_directory'). '/images/img-article-body-section2.png',
                        get_bloginfo('template_directory'). '/images/img-article-slider-section1.png',
                        get_bloginfo('template_directory'). '/images/img-article-body-section3.png',
                        get_bloginfo('template_directory'). '/images/img-article-body-section2.png',
                        get_bloginfo('template_directory'). '/images/img-article-slider-section1.png',
                        get_bloginfo('template_directory'). '/images/img-article-body-section3.png',
                    ];
                    foreach($image as $idx => $row){
                        echo "<li  data-thumb='" . $row . "' data-src='" . $row . "'><img src='" . $row ."'>
                            <div class='caption'>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam suscipit vehicula aliquam</p>
                            </div>
                        </li>";
                    } 
                    ?>
                    <style>
                    .caption{
                        margin-top: -56px; 
                        margin-left: 7px;
                        color: #fffff;
                    }
                    </style>
                </ul>
                <div class="text-right" style="margin:20px auto">
                    <a href='#' class="btn btn-black">VIEW MORE</a>
                </div>
            </div>
            <!-- slider 1 end -->
            <div id="section2-parent" class="column is-full">  
                <div id="section2" class="container is-fullhd" style="background-image: url('<?php echo get_bloginfo('template_directory'); ?>/images/img-body-section2-bg.png');">
                    <div  id="mask-opacity2" class="container is-fullhd">
                        <h1 class="section2-title">ADD A TAGLINE/SLOGAN</h1> 
                    </div>
                 </div>          
            </div>

            <div id="section2-sub1" class="column is-full">
                <h1 class="section2-title">Lorem Ipsum</h1>
                <br>
                <br>
                <p class="text-section-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p class="text-section-2-right">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            
            <!-- Slider 2 start -->
            <div id="slider-section2" >
                <h1 class="heading-slider text-right">Featured Article</h1>
                <ul class="lightSlider">
                    <?php 

                    $image = [
                        get_bloginfo('template_directory'). '/images/img-article-body-section2.png',
                        get_bloginfo('template_directory'). '/images/img-article-slider-section1.png',
                        get_bloginfo('template_directory'). '/images/img-article-body-section3.png',
                        get_bloginfo('template_directory'). '/images/img-article-body-section2.png',
                        get_bloginfo('template_directory'). '/images/img-article-slider-section1.png',
                        get_bloginfo('template_directory'). '/images/img-article-body-section3.png',
                    ];
                    foreach($image as $idx => $row){
                        echo "<li  data-thumb='" . $row . "' data-src='" . $row . "'><img src='" . $row ."'></li>";
                    } 
                    ?>
                </ul>
                <div class="text-right" style="margin:20px auto">
                    <a href='#' class="btn btn-black">VIEW MORE</a>
                </div>
            </div>
                <!-- slider 2 end -->

            <div id="section3-parent" class="column is-fullhd">
                <div id="section3" class="container is-fullhd">
                        <iframe width="100%" height="600px" src="https://www.youtube.com/embed/qkssJOfwz9Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            
            <article id="media"  class="media">
                    <div  id="media-left" class="media-left">
                            <iframe width="350" height="200" src="https://www.youtube.com/embed/qkssJOfwz9Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div  id="media-center" class="right">
                      
                            <iframe width="350" height="200" src="https://www.youtube.com/embed/qkssJOfwz9Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                      </div>
                      
                    </div>
                    <div id="media-right" class="media-right">
                            <iframe width="350" height="200" src="https://www.youtube.com/embed/qkssJOfwz9Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
            </article>

            <article id="media"  class="media">
                    <div  id="media-left" class="media-left">
                            <iframe width="350" height="200" src="https://www.youtube.com/embed/qkssJOfwz9Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div  id="media-center" class="right">
                      
                            <iframe width="350" height="200" src="https://www.youtube.com/embed/qkssJOfwz9Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                      </div>
                      
                    </div>
                    <div id="media-right" class="media-right">
                            <iframe width="350" height="200" src="https://www.youtube.com/embed/qkssJOfwz9Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
            </article>

            <article id="media"  class="media">
                    <div  id="media-left" class="media-left">
                            <iframe width="350" height="200" src="https://www.youtube.com/embed/qkssJOfwz9Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div  id="media-center" class="right">
                      
                            <iframe width="350" height="200" src="https://www.youtube.com/embed/qkssJOfwz9Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                      </div>
                      
                    </div>
                    <div id="media-right" class="media-right">
                            <iframe width="350" height="200" src="https://www.youtube.com/embed/qkssJOfwz9Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
            </article>

            <div  id="button-section3" class="button">View More</div>
<script src="<?php echo get_bloginfo('template_directory'); ?>/plugin/lightslider-master/dist/js/lightslider.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/plugin/lightslider-master/dist/js/lightgallery-all.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $("#header,#background").width($(window).width());
    $("#header,#background").height($(window).height());

    $("#home-chevron-down").on('click', function(){
        $("html, body").animate({ scrollTop: $("#navbar-home").offset().top });
    })

    $('.lightSlider').lightSlider({
        item:3,
        loop:false,
        slideMove:2,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:600,
        pager:false,
        enableDrag: false,
        slideMargin: 2,
        gallery:true,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:-1,
                  }
            },
            {
                breakpoint:480,
                settings: {
                    item:2,
                    slideMove:1
                  }
            }
        ],
        onSliderLoad: function(el) {
            el.lightGallery({
                selector: '.lightSlider .lslide'
            });
            el.addClass('no-margin-slider');
            $(".no-margin-slider").closest(".lSSlideOuter").addClass("padding-right-slider");
            $(el).closest($(".lSSlideWrapper")).height( $(el).closest($(".lightSlider")).height());
        },
        prevHtml: "<i class='fa fa-chevron-left'></i>",
        nextHtml: "<i class='fa fa-chevron-right'></i>",
    });  
  });
</script>
<?php get_footer(); ?>