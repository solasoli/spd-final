<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_bloginfo('template_directory'); ?>/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/css/footer.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?php echo get_bloginfo('template_directory'); ?>/js/main.js"></script>

</head>
<body>
    <!-- header/navbar start -->
    <div id="header" class="column is-full">
        <div id='home-chevron-down'><i class="fa fa-chevron-down"></i></div>
        <div  id="background" class="container is-fullhd"  style="background-image: url('<?php echo get_bloginfo('template_directory'); ?>/images/img-landing-header.png');">
         <div class="container">
            <img  class="logo" src="<?php echo get_bloginfo('template_directory'); ?>/images/logo-sc.png">
        </div>
    </div>
</div> 
<div  id="navbar-home" class="navbar is-expanded">

    <div  id="navbar-attr1" class="navbar-item">
        <a href="#">ARTICLES</a>
    </div>
    <div id="navbar-attr2" class="navbar-item">
        <a href="#">COMMUNITY</a>
    </div>
    <div id="navbar-attr3" class="navbar-item">
        <a href="#">WORKSHOP</a>
    </div>
    <div id="navbar-attr4" class="navbar-item">
        <a href="#">MARKETPLACE</a>
    </div>
    <div id="navbar-attr5" class="navbar-item">
        <a href="#" class="modal-button" data-target="modal" >REGISTER &nbsp;| &nbsp; LOG IN</a>
    </div>
    
</div>
<!-- header/navbar end -->
<div id="modal" class="modal">
  <div class="modal-background"></div>
  <div class="modal-content">
    <div class="box">
        <div class="columns">
            <div class="column">
                LOGIN SECTION
            </div>
            <div class="column">
                REGISTER SECTION
            </div>
        </div>
    </div>
</div>
<button class="modal-close is-large" aria-label="close"></button>
</div>

<script type="text/javascript">
    $(function(){
        $(document).on('click',".modal-button", function(){
            var target = $(this).data('target');
            $("#" + target).addClass("is-active");
        })


        $(document).on('click',".modal-close", function(){
            $(this).closest($(".modal")).removeClass("is-active");
        })
    })
</script>