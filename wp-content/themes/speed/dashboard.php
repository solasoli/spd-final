<?php

/* Template Name: Dashboard */ 

?>

<?php get_header('dashboard');?>
    <div class="container">
        <div class="columns">
            <div class="column is-8">

                <!-- Request Referral -->
                <div class="columns box-content">
                    <div class='column'>

                        <div class="media">
                            <div class="media-left">
                                <figure class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image" class="is-rounded">
                                </figure>
                            </div>
                            <div class="media-content">
                                <div class="content">
                                    <h2 class="title">James Doe</h2>
                                    <p class="subtitle">
                                        <small>123 Review</small> 
                                        <small>455 Subscriber</small>
                                    </p>
                                    <p>
                                        <div class="field">
                                            <div class="control">
                                                <input class="input" type="email" placeholder="Write your thoughts">
                                            </div>
                                            <br>    
                                            <button class="button is-black ">Request a Referral</button>
                                        </div>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- End Request Referral -->

                <!-- Post -->
                <div class="columns box-content">
                    <div class='column'>

                        <div class="media">
                            <div class="media-left">
                                <figure class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image" class="is-rounded">
                                </figure>
                            </div>
                            <div class="media-content">
                                <div class="content">
                                    <h2 class="title">
                                        James Doe

                                        <a href="#" class="button is-black is-pulled-right is-small">Unfollow</a>
                                    </h2>

                                    <p class="subtitle">
                                        <small>123 Review</small> 
                                        <small>455 Subscriber</small>
                                        <small>3 hours ago</small>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-content">
                                <p>
                                    Lorem ipsum sit amet dolor Lorem ipsum sit amet dolor Lorem ipsum sit amet dolor Lorem ipsum sit amet dolor Lorem ipsum sit amet dolor Lorem ipsum sit amet dolor Lorem ipsum sit amet dolor .
                                    <br>
                                    <br>
                                    Lorem ipsum sit amet dolor Lorem ipsum sit amet dolor Lorem ipsum sit amet dolor Lorem ipsum sit amet dolor Lorem ipsum sit amet dolor Lorem ipsum sit amet dolor 
                                </p>

                                <nav class="level is-mobile">
                                    <div class="level-left"></div>
                                    <div class="level-right">
                                        <a class="level-item" aria-label="reply">
                                            <span class="icon is-small is-black">
                                              <i class="fas fa-thumbs-up" aria-hidden="true"></i>
                                            </span>
                                            &nbsp; 7 Likes
                                        </a>
                                        <a class="level-item" aria-label="like">
                                            <span class="icon is-small is-black">
                                              <i class="fas fa-share" aria-hidden="true"></i>
                                            </span>
                                            &nbsp; 2 Shares
                                        </a>
                                        <a class="level-item" aria-label="retweet">
                                            <span class=" is-black">
                                              17 Comments
                                            </span>
                                        </a>
                                    </div>
                                </nav>

                                <nav class="level is-mobile">
                                    <div class="level-left">
                                        <a class="level-item" aria-label="reply">
                                            <span class="icon is-small is-black">
                                              <i class="fas fa-thumbs-up" aria-hidden="true"></i>
                                            </span>
                                            &nbsp; Like
                                        </a>
                                        <a class="level-item" aria-label="retweet">
                                            <span class="icon is-small is-black">
                                              <i class="fas fa-exclamation-triangle" aria-hidden="true"></i>
                                            </span>
                                            &nbsp; Report
                                        </a>
                                        <a class="level-item" aria-label="like">
                                            <span class="icon is-small is-black">
                                              <i class="fas fa-share" aria-hidden="true"></i>
                                            </span>
                                            &nbsp; Share
                                        </a>
                                    </div>
                                </nav>

                                <div class="control">
                                    <input class="input" type="email" placeholder="Write your comments">
                                </div>

                                <br>
                                
                                <p><strong>MOST POPULAR</strong></p>

                                <!-- Comments -->
                                <div class="media">
                                    <div class="media-left">
                                        <figure class="image is-64x64">
                                            <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image" class="is-rounded">
                                        </figure>
                                    </div>
                                    <div class="media-content">
                                        <div class="content">
                                            <div class="box comment">
                                                Hello youtube
                                            </div>
                                            <nav class="level is-mobile">
                                                <div class="level-left">
                                                    <a class="level-item" aria-label="reply">
                                                        &nbsp; Like
                                                    </a>
                                                    <a class="level-item" aria-label="retweet">
                                                        &nbsp; Reply
                                                    </a>
                                                    <a class="level-item" aria-label="like">
                                                        &nbsp; Share
                                                    </a>
                                                </div>
                                                <div class="level-right">
                                                    <a class="level-item" aria-label="reply">
                                                        <span class="icon is-small is-black">
                                                          <i class="fas fa-thumbs-up" aria-hidden="true"></i>
                                                        </span>
                                                        &nbsp; 7 Likes
                                                    </a>
                                                    <a class="level-item" aria-label="like">
                                                        <span class="icon is-small is-black">
                                                          <i class="fas fa-share" aria-hidden="true"></i>
                                                        </span>
                                                        &nbsp; 2 Shares
                                                    </a>
                                                </div>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                                <!-- end comments -->

                            </div>
                        </div>

                    </div>
                </div>
                <!-- End Post -->

            </div>

            <!-- sidebar -->
            <div class="sidebar column">

                <!-- global search -->
                <div class="columns box-content">

                    <div class='column'>
                        <div class="field">
                          <div class="control has-icons-left has-icons-right">
                            <input class="input" type="email" placeholder="Search">
                            <span class="icon is-small is-left">
                              <i class="fas fa-envelope"></i>
                            </span>
                            <span class="icon is-small is-right">
                              <i class="fas fa-search"></i>
                            </span>
                          </div>
                        </div>
                    </div>


                </div>
                <!-- end global search -->

                <!-- Trending Topic -->
                <div class="columns box-content">
                    <div class="column">
                        <h1 class="box-title">TRENDING TOPIC</h1>

                        <!-- List Topic -->
                        <div class="columns">
                            
                            <article class="media">
                                <div class="media-left">
                                    <figure class="image is-64x64">
                                        <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <div class="content">
                                        <p>
                                            <strong>Lorem Ipsum</strong> 
                                            
                                            
                                            <br>
                                            
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                        </p>
                                    </div>
                                </div>
                            </article>

                        </div>
                        <!-- End List Topic -->

                        <!-- List Topic -->
                        <div class="columns">
                            
                            <article class="media">
                                <div class="media-left">
                                    <figure class="image is-64x64">
                                        <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <div class="content">
                                        <p>
                                            <strong>Lorem Ipsum</strong> 
                                            
                                            
                                            <br>
                                            
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                        </p>
                                    </div>
                                </div>
                            </article>

                        </div>
                        <!-- End List Topic -->

                        <!-- List Topic -->                        
                        <div class="columns">
                            
                            <article class="media">
                                <div class="media-left">
                                    <figure class="image is-64x64">
                                        <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <div class="content">
                                        <p>
                                            <strong>Lorem Ipsum</strong> 
                                            
                                            
                                            <br>
                                            
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                        </p>
                                    </div>
                                </div>
                            </article>

                        </div> 
                        <!-- End List Topic -->

                        <!-- List Topic -->
                        <div class="columns">
                            
                            <article class="media">
                                <div class="media-left">
                                    <figure class="image is-64x64">
                                        <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <div class="content">
                                        <p>
                                            <strong>Lorem Ipsum</strong> 
                                            
                                            
                                            <br>
                                            
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                        </p>
                                    </div>
                                </div>
                            </article>

                        </div>
                        <!-- End List Topic -->

                    </div>
                </div>
                <!-- End Trending Topic -->

                <!-- Top Member -->
                <div class="columns box-content">
                    <div class="column">
                        <h1 class="box-title">TOP MEMBER OF THIS WEEK</h1>

                        <!-- List Top Member -->
                        <div class="columns">
                            
                            <article class="media">
                                <div class="media-left">
                                    <figure class="image is-64x64">
                                        <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image" class="is-rounded">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <div class="content">
                                        <p>
                                            <strong>John Smith</strong> 
                                            
                                            
                                            <br>
                                            
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                        </p>
                                    </div>

                                    <a href="#" class="button is-black is-pulled-right is-small">Follow</a>
                                </div>
                            </article>

                        </div>
                        <!-- End List Top Member -->

                        <!-- List Top Member -->
                        <div class="columns">
                            
                            <article class="media">
                                <div class="media-left">
                                    <figure class="image is-64x64">
                                        <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image" class="is-rounded">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <div class="content">
                                        <p>
                                            <strong>John Smith</strong> 
                                            
                                            
                                            <br>
                                            
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                        </p>
                                    </div>

                                    <a href="#" class="button is-black is-pulled-right is-small">Follow</a>
                                </div>
                            </article>

                        </div>
                        <!-- End List Top Member -->

                        <!-- List Top Member -->                        
                        <div class="columns">
                            
                            <article class="media">
                                <div class="media-left">
                                    <figure class="image is-64x64">
                                        <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image" class="is-rounded">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <div class="content">
                                        <p>
                                            <strong>John Smith</strong> 
                                            
                                            
                                            <br>
                                            
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                        </p>
                                    </div>

                                    <a href="#" class="button is-black is-pulled-right is-small">Follow</a>
                                </div>
                            </article>

                        </div> 
                        <!-- End List Top Member -->

                        <!-- List Top Member -->
                        <div class="columns">
                            
                            <article class="media">
                                <div class="media-left">
                                    <figure class="image is-64x64">
                                        <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image" class="is-rounded">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <div class="content">
                                        <p>
                                            <strong>John Smith</strong> 
                                            
                                            
                                            <br>
                                            
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                        </p>
                                    </div>

                                    <a href="#" class="button is-black is-pulled-right is-small">Follow</a>
                                </div>
                            </article>

                        </div>
                        <!-- End List Top Member -->

                    </div>
                </div>
                <!-- End Top Member -->

            </div>
            <!-- End sidebar -->
        </div>
    </div>
</div>
<?php get_footer(); ?>