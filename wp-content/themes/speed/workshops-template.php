<?php

/* Template Name: Workshops Template */ 

?>

<?php get_header();?>
<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/css/workshop-template.css">
<?=do_shortcode('[gmw_current_location]');?>
<div class="columns">
        <!-- left column -->
        <div class="column is-8">
            <!-- list of workshop profile -->
            <div class="card-profile-area">
                    <div class="card">
                <?php
                $args = array(
                    'post_type' => 'workshop',
                    'post_status' => 'publish',
                    'posts_per_page' => -1
                );
                $posts = new WP_Query( $args );
                while ( $posts->have_posts() ) : $posts->the_post();
                ?>

                    <div class="card-content">
                        <div class="profile-bio">
                                <p class="title">
                                    <div class="media">
                                        <div class="media-left">
                                                <strong><?=the_title();?></strong>
                                        </div>
                                        <div class="media-right">
                                                <div id="review-socore" class="container" >
                                                    <style>
                                                    #review-socore{
                                                        margin-left: 500px;
                                                    }
                                                    </style>
                                                        <p><strong><?php if(function_exists('the_ratings')) { the_ratings('rating_images'); } ?></strong></p>
                                                        
                                                </div>
                                                <div class="review-nominal">
                                                        <p><strong>300 reviews</strong></p>
                                                </div>
                                                <style>
                                                .review-nominal{
                                                    margin-left: 588px;
                                                }
                                                </style>
                                                        
                                        </div>
                                    </div>
                                        
                                </p>
                            <p class="address">
                                  <strong>Address:</strong> <?=the_field('address');?>
                            </p>
                            <p><strong>Hours:</strong> Open Now, Closes 9 PM</p>
                                
                                <div class="phone">
                                    <p><strong>Phone&nbsp;&nbsp;:</strong>&nbsp; <?=the_field('phone');?></p>
                                </div>
                                <div class="service">
                                    <p><strong>Service&nbsp;&nbsp;:</strong>&nbsp; <?=the_field('service');?></p>
                                </div>
                                <!-- 
                                <div id="review-socore" class="container" >
                                    <p><strong>4.8</strong></p>
                                </div> -->
                                <ul class ="lightSlider">
                                    <?php 

                                    $image = get_field('add_image');
                                    foreach($image as $idx => $row){
                                        echo "<li  data-thumb='" . $row['sizes']['large'] . "' data-src='" . $row['url'] . "'><img src='" . $row['sizes']['large'] ."'></li>";
                                    } 
                                    ?>
                                </ul>
                                
                        </div>
                    </div>
                    <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                </div>
            </div>
                
            <!-- end of list of workshop profile -->
        </div>
        <!-- end of left column -->

        <!-- right column -->
        <div class="sidebar">
                <div class="column">
                  <div class="card">
                    <div class="card-content">
                      <p class="sidebar1-title">
                        <strong>CHECK OTHER GARAGE</strong>
                      </p>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <figure class="image">
                                <img  class="sidebar-image" src="https://via.placeholder.com/150x100.png" alt="Placeholder image">
                          </figure>
                        </div>
                        <div class="media-right">
                            <p><strong>Lorem Ipsum</strong></p>
                            <div id="sidebar-content" class="content" >
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </div>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <figure class="image">
                                <img  class="sidebar-image" src="https://via.placeholder.com/150x100.png" alt="Placeholder image">
                          </figure>
                        </div>
                        <div class="media-right">
                            <p><strong>Lorem Ipsum</strong></p>
                            <div id="sidebar-content" class="content" >
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </div>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <figure class="image">
                                <img  class="sidebar-image" src="https://via.placeholder.com/150x100.png" alt="Placeholder image">
                          </figure>
                        </div>
                        <div class="media-right">
                            <p><strong>Lorem Ipsum</strong></p>
                            <div id="sidebar-content" class="content" >
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </div>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <figure class="image">
                                <img  class="sidebar-image" src="https://via.placeholder.com/150x100.png" alt="Placeholder image">
                          </figure>
                        </div>
                        <div class="media-right">
                            <p><strong>Lorem Ipsum</strong></p>
                            <div id="sidebar-content" class="content" >
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </div>
                        </div>
                    </div>
                  </div>
                  </div>
          
                <div class="sidebar2">
                    <div class="card">
                        <div class="card-content">
                            <div class="media">
                                <div class="media-left">
                                    <p class="image is-96x96">
                                        <img class="is-rounded" src="https://via.placeholder.com/96x96.png">
                                        <span class="icon has-text-success">
                                            <i  id="icon-image" class="fas fa-check-circle"></i>
                                      </span>
                                    </p>
                                </div>
                                <div class="media-right">
                                    <p><strong>John Doe</strong></p>
                                        <div  id="sidebar-content" class="content" >
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        <a  id="follow-sidebar-button" class="button is-black is-small">Follow</a>
                                    </div>
          
                                </div>
                                
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <p class="image is-96x96">
                                        <img class="is-rounded" src="https://via.placeholder.com/96x96.png">
                                        <span class="icon has-text-success">
                                            <i  id="icon-image" class="fas fa-check-circle"></i>
                                      </span>
                                    </p>
                                </div>
                                <div class="media-right">
                                    <p><strong>John Doe</strong></p>
                                        <div  id="sidebar-content" class="content" >
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        <a  id="follow-sidebar-button" class="button is-black is-small">Follow</a>
                                    </div>
          
                                </div>
                                
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <p class="image is-96x96">
                                        <img class="is-rounded" src="https://via.placeholder.com/96x96.png">
                                        <span class="icon has-text-success">
                                            <i  id="icon-image" class="fas fa-check-circle"></i>
                                      </span>
                                    </p>
                                </div>
                                <div class="media-right">
                                    <p><strong>John Doe</strong></p>
                                        <div  id="sidebar-content" class="content" >
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        <a  id="follow-sidebar-button" class="button is-black is-small">Follow</a>
                                    </div>
          
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <!-- end of right column -->
    </div>
    <script type="text/javascript" src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/plugin/lightslider-master/dist/js/lightslider.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/plugin/lightslider-master/dist/js/lightgallery-all.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.lightSlider').lightSlider({
        item:4,
        loop:false,
        slideMove:2,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:600,
        pager:false,
        enableDrag: false,
         gallery:true,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:-1,
                  }
            },
            {
                breakpoint:480,
                settings: {
                    item:2,
                    slideMove:1
                  }
            }
        ],
        onSliderLoad: function(el) {
            el.lightGallery({
                selector: '#lightSlider .lslide'
            });
            el.addClass('no-margin-slider');
            $(".no-margin-slider").closest(".lSSlideOuter").addClass("padding-right-slider");
        },
        prevHtml: "<i class='fa fa-chevron-left'></i>",
        nextHtml: "<i class='fa fa-chevron-right'></i>"
    });  
  });
</script>
    <?php get_footer(); ?>