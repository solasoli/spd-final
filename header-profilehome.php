<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_bloginfo('template_directory'); ?>/css/style.css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="<?php echo get_bloginfo('template_directory'); ?>/js/main.js"></script>

    </head>
    <body>
        <!-- header/navbar start -->
        <div id="header" class="column is-full">
            
               <div class="container">
                    <img  class="logo" src="<?php echo get_bloginfo('template_directory'); ?>/images/logo-sc.png">
               </div>
            
        </div> 

        <div  id="navbar-home" class="navbar is-expanded">
            
                    <div  id="navbar-attr1" class="navbar-item">
                            <a href="#">ARTICLES</a>
                    </div>
                    <div id="navbar-attr2" class="navbar-item">
                            <a href="#">COMMUNITY</a>
                    </div>
                    <div id="navbar-attr3" class="navbar-item">
                            <a href="#">WORKSHOP</a>
                    </div>
                    <div id="navbar-attr4" class="navbar-item">
                            <a href="#">MARKETPLACE</a>
                    </div>
                    <div id="navbar-attr5" class="navbar-item">
                            <a href="#">REGISTER</a>&nbsp;|&nbsp;<a href="#">LOG IN</a>
                    </div>
            
        </div>
        <!-- header/navbar end -->